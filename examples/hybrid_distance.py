import banquo as bq

predicates = {
    "p1": bq.HybridPredicate("1.0*x <= 10.0", "s1"),
    "p2": bq.HybridPredicate("1.0*x <= 10.0", "s2"),
}

guards = {
    ("s1", "s2"): ["-1.0*x <= -4.0"],
    ("s2", "s1"): ["1.0*x <= 4.0"],
}

trace = {
    0: ({"x": 4.0}, "s2"),
    2: ({"x": 3.8}, "s1"),
    4: ({"x": 3.6}, "s1"),
    6: ({"x": 3.9}, "s1"),
    8: ({"x": 4.2}, "s2"),
    10: ({"x": 4.1}, "s2"),
}

(hdist, rob) = bq.hybrid_distance(r"[] (p1 /\ p2)", predicates, guards, trace)
