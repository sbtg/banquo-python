import typing

import banquo as bq
import pytest as pt

Trace = typing.Dict[int, typing.Dict[str, float]]


@pt.fixture
def trace() -> Trace:
    entries = [
        (0, 0.0),
        (0.3947, 0.5881),
        (0.7587, 1.1068),
        (1.0660, 1.4967),
        (1.2998, 1.7169),
        (1.4546, 1.7508),
        (1.5377, 1.6075),
        (1.5675, 1.3204),
        (1.5708, 0.9412),
        (1.5787, 0.5313),
        (1.6216, 0.1525),
        (1.7242, -0.1431),
        (1.9019, -0.3207),
        (2.1583, -0.368),
        (2.4844, -0.2963),
        (2.8603, -0.1383),
        (3.2583, 0.0582),
        (3.6471, 0.2386),
        (3.9968, 0.3511),
        (4.2840, 0.3561),
        (4.4947, 0.2326),
        (4.6273, -0.017),
        (4.6925, -0.3667),
        (4.7114, -0.7708),
        (4.7128, -1.1705),
        (4.7280, -1.5029),
        (4.7861, -1.7113),
        (4.9095, -1.7537),
        (5.1104, -1.6104),
        (5.3886, -1.2874),
        (5.7317, -0.816),
    ]

    return {time: {"x": value} for time, value in entries}


P1 = "-1.0*x <= 2.0"
P2 = "1.0*x <= 2.0"


def test_01(trace: Trace):
    formula = f"{P1} or {P2}"
    robustness = bq.robustness(formula, trace)
    expected = 2.0

    assert robustness == pt.approx(expected, abs=1e-4)


def test_02(trace: Trace):
    formula = f"<> {P1}"
    robustness = bq.robustness(formula, trace)
    expected = 3.7508

    assert robustness == pt.approx(expected, abs=1e-4)
