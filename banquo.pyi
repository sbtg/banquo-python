def robustness(formula: str, trace: dict[float, dict[str, float]] | list[tuple[float, dict[str, float]]]) -> float: ...

class HybridPredicate:
    def __init__(self, predicate: str | None, location: str): ...

def hybrid_distance(
    formula: str,
    predicates: dict[str, HybridPredicate],
    guards: dict[tuple[str, str], list[str]],
    trace: dict[float, tuple[dict[str, float], str]] | list[tuple[float, tuple[dict[str, float], str]]],
) -> tuple[float, float]: ...
