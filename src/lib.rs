use std::collections::hash_map::DefaultHasher;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};
use std::iter::FromIterator;

use banquo::automaton::{Automaton, Guard};
use banquo::expressions::{HybridPredicate, Predicate};
use banquo::formulas::{HybridDistance, PathGuardDistance, eval_robustness, eval_hybrid_dist};
use banquo::parser::{parse_formula, parse_hybrid_formula, parse_predicate};
use banquo::Trace;
use pyo3::exceptions::{PyRuntimeError, PyValueError};
use pyo3::prelude::*;
use pyo3::types::{PyDict, PyList};

type VariableMap = HashMap<String, f64>;

struct PyTrace(Trace<VariableMap>);

fn read_dict_elements<'a, S>(dict: &'a PyDict) -> PyResult<Vec<(f64, S)>>
where
    S: FromPyObject<'a>
{
    let n_values = dict.len();
    let mut values: Vec<(f64, S)> = Vec::with_capacity(n_values);

    for key in dict.keys() {
        let time: f64 = key.extract()?;
        let state = dict.get_item(key).unwrap().extract()?;
        values.push((time, state));
    }

    Ok(values)
}

impl<'source> FromPyObject<'source> for PyTrace {
    fn extract(ob: &'source PyAny) -> PyResult<Self> {
        if let Ok(dict) = <PyDict as PyTryFrom>::try_from(ob) {
            let values = read_dict_elements(dict)?;
            let trace = Trace::from_iter(values);

            return Ok(PyTrace(trace));
        }

        if let Ok(list) = <PyList as PyTryFrom>::try_from(ob) {
            let values: Vec<(f64, VariableMap)> = list.extract()?;
            let trace = Trace::from_iter(values);

            return Ok(PyTrace(trace));
        }

        let message = format!("could not convert {} into Trace", ob.get_type());
        Err(PyValueError::new_err(message))
    }
}

struct PyHybridTrace(Trace<(VariableMap, u64)>);

fn convert_pytrace<I>(elements: I) -> Trace<(VariableMap, u64)>
where
    I: IntoIterator<Item = (f64, (VariableMap, String))>
{
    let convert_state_location = |(time, (state, location)): (f64, (VariableMap, String))| {
        let mut hasher = DefaultHasher::new();
        location.hash(&mut hasher);

        (time, (state, hasher.finish()))
    };

    elements
        .into_iter()
        .map(convert_state_location)
        .collect()
}

impl<'source> FromPyObject<'source> for PyHybridTrace {
    fn extract(ob: &'source PyAny) -> PyResult<Self> {
        if let Ok(dict) = <PyDict as PyTryFrom>::try_from(ob) {
            let values = read_dict_elements(dict)?;
            let trace = convert_pytrace(values);

            return Ok(PyHybridTrace(trace));
        }

        if let Ok(list) = <PyList as PyTryFrom>::try_from(ob) {
            let values: Vec<(f64, (VariableMap, String))> = list.extract()?;
            let trace = convert_pytrace(values);

            return Ok(PyHybridTrace(trace));
        }

        let message = format!("could not convert {} into hybrid Trace", ob.get_type());
        Err(PyValueError::new_err(message))
    }
}

#[derive(Clone)]
#[pyclass(name = "HybridPredicate")]
struct PyHybridPredicate {
    predicate: Option<Predicate>,
    location: String,
}

#[pymethods]
impl PyHybridPredicate {
    #[new]
    fn new(predicate_formula: Option<&str>, location: &str) -> PyResult<Self> {
        let maybe_predicate = predicate_formula
            .map(|phi| parse_predicate(phi).map_err(|err| PyRuntimeError::new_err(err.to_string())))
            .transpose();

        let hybrid_predicate = Self {
            predicate: maybe_predicate?,
            location: location.to_owned(),
        };

        Ok(hybrid_predicate)
    }
}

impl PyHybridPredicate {
    fn location_hash(&self) -> u64 {
        let mut hasher = DefaultHasher::new();
        self.location.hash(&mut hasher);

        hasher.finish()
    }
}

#[pyfunction]
fn robustness(formula: &str, trace: PyTrace) -> PyResult<f64> {
    let parsed = parse_formula(formula).map_err(|err| PyRuntimeError::new_err(err.to_string()))?;

    let robustness_value = eval_robustness(parsed, &trace.0)
        .map_err(|err| PyRuntimeError::new_err(err.to_string()))?;

    Ok(robustness_value)
}

fn parse_edge_guard(formulas: Vec<String>) -> PyResult<Guard> {
    let parse_guard_predicate = |formula: String| -> PyResult<Predicate> {
        parse_predicate(&formula).map_err(|err| {
            let message = format!("Could not parse guard predicate {}: {}", formula, err);
            PyValueError::new_err(message)
        })
    };

    let predicates = formulas
        .into_iter()
        .map(parse_guard_predicate)
        .collect::<Result<Vec<_>, _>>();

    Ok(Guard::from_iter(predicates?))
}

fn hash_edge((s1, s2): (String, String)) -> (u64, u64) {
    let mut s1_hasher = DefaultHasher::new();
    s1.hash(&mut s1_hasher);

    let mut s2_hasher = DefaultHasher::new();
    s2.hash(&mut s2_hasher);

    (s1_hasher.finish(), s2_hasher.finish())
}

#[pyfunction]
fn hybrid_distance(
    formula: &str,
    predicates: HashMap<String, PyHybridPredicate>,
    edges: HashMap<(String, String), Vec<String>>,
    trace: PyHybridTrace,
) -> PyResult<(f64, f64)> {
    let guards = edges
        .into_iter()
        .map(|(edge, predicates)| parse_edge_guard(predicates).map(|guard| (hash_edge(edge), guard)))
        .collect::<Result<HashMap<_, _>, _>>();

    let automaton = Automaton::from(guards?);
    let hybrid_predicates = predicates
        .into_iter()
        .map(|(name, data)| {
            let hash = data.location_hash();
            let predicate = HybridPredicate::new(data.predicate, hash, &automaton);
            (name, predicate)
        })
        .collect();

    let parsed = parse_hybrid_formula(formula, hybrid_predicates)
        .map_err(|_| PyValueError::new_err("Could not parse formula"))?;

    let hybrid_distance = eval_hybrid_dist(parsed, &trace.0)
        .map_err(|err| PyRuntimeError::new_err(err.to_string()))?;

    let py_distance = match hybrid_distance {
        HybridDistance::Infinite => (f64::INFINITY, f64::INFINITY),
        HybridDistance::PathDistance(PathGuardDistance { path_distance: pd, guard_distance: gd }) => (pd as f64, gd),
        HybridDistance::Robustness(rob) => (0f64, rob),
    };

    Ok(py_distance)
}

#[pymodule]
#[pyo3(name = "banquo")]
fn banquo_python(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_class::<PyHybridPredicate>()?;
    m.add_function(wrap_pyfunction!(robustness, m)?)?;
    m.add_function(wrap_pyfunction!(hybrid_distance, m)?)?;

    Ok(())
}
